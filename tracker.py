from __future__ import division



class tracker (object):

  __slots__ = 'xtol', 'vtol', 'a', 'v', 'x', 'y'

  def __init__ (self, a = 0, xtol = 0, vtol = 0):
    self.xtol = xtol
    self.vtol = vtol

    self.a = a
    self.v = 0
    self.x = 0
    self.y = 0

  def __str__ (self):
    return '(%f -> %f)' % (self.y, self.x)

  __repr__ = __str__

  def __add__ (self, other):
    return self.y + float (other)

  def __sub__ (self, other):
    return self.y - float (other)

  def __mul__ (self, other):
    return self.y * float (other)

  def __floordiv__ (self, other):
    return self.y // float (other)

  def __mod__ (self, other):
    return self.y % float (other)

  def __divmod__ (self, other):
    return divmod (self.y, float (other))

  def __pow__ (self, other):
    return self.y ** float (other)

  def __truediv__ (self, other):
    return self.y / float (other)

  __div__ = __truediv__

  def __radd__ (self, other):
    return float (other) + self.y

  def __rsub__ (self, other):
    return float (other) - self.y

  def __rmul__ (self, other):
    return float (other) * self.y

  def __rfloordiv__ (self, other):
    return float (other) // self.y

  def __rmod__ (self, other):
    return float (other) % self.y

  def __rdivmod__ (self, other):
    return divmod (float (other), self.y)

  def __rpow__ (self, other):
    return float (other) ** self.y

  def __rtruediv__ (self, other):
    return float (other) / self.y

  __rdiv__ = __rtruediv__

  def __iadd__ (self, other):
    self.y += float (other)

  def __isub__ (self, other):
    self.y -= float (other)

  def __imul__ (self, other):
    self.y *= float (other)

  def __ifloordiv__ (self, other):
    self.y //= float (other)

  def __imod__ (self, other):
    self.y %= float (other)

  def __ipow__ (self, other):
    self.y **= float (other)

  def __itruediv__ (self, other):
    self.y /= float (other)

  __idiv__ = __itruediv__

  def __neg__ (self):
    return -self.y

  def __pos__ (self):
    return self.y

  def __abs__ (self):
    return abs (self.y)

  def __float__ (self):
    return self.y

  def update (self, dt):
    a = self.a
    v = self.v
    x = self.x
    y = self.y

    if x < y:
      a = -a

    try:
      from math import sqrt
      disc = sqrt (v * v / 2 + a * (x - y))

      if x < y:
        t = (-v - disc) / a
      else:
        t = (-v + disc) / a
    except ZeroDivisionError:
      pass

    if t >= dt:
      dv = a * dt
      self.y += (v + dv / 2) * dt
      self.v += dv
    elif t >= 0:
      dv = a * t
      self.y += (v + dv / 2) * t
      self.v += dv
      dt -= t
      dv = -a * dt
      self.y += (self.v + dv / 2) * dt
      self.v += dv
    else:
      dv = -a * dt
      self.y += (v + dv / 2) * dt
      self.v += dv

    if abs (self.x - self.y) < self.xtol and abs (self.v) < self.vtol:
      self.y = self.x
      self.v = 0
