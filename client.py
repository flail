import threading
import socket
import select
from struct import pack, unpack, calcsize

from pyglet.image import load

from geom import vector
from egg import egg

class client (threading.Thread):
    def __init__(self, lev = None , egg = None, host='localhost', port=30000):
        threading.Thread.__init__ (self)
        self.hostname = host
        self.portnum = port
        self.level = lev
        self.local_egg = egg
        self.remote_eggs = {}
        self.running = True
        self.tex = load ('data/egg.png').texture

    def run(self):
        s = None
        try:
            s = socket.socket (socket.AF_INET, socket.SOCK_STREAM)
            s.setsockopt (socket.SOL_TCP, socket.TCP_NODELAY, 0)
            s.connect ((self.hostname,self.portnum))
            
            s.send ('FlailClient')
            
            data = s.recv (1024)
            
            if data == 'FlailServer':
                s.send ('READY')
            else:
                print 'FlailClient: Closing connection'
                s.close ()
                s = None
        except socket.timeout:
            s.close ()
            s = None
            return 
        except socket.error:
            s.close ()
            s = None
            return 

        if s:
          print 'Connected To Server'
          self.running = True
        else:
          return

        while self.running:
            inputs, outputs, excpts = select.select ([s], [], [], 0.5)
            
            for i in inputs:
                if i == s:
                    # do update stuff
                    data = s.recv (packet_size ())

                    if not data:
                        s.close ()
                        return

                    # if we didn't get all the data, get more
                    # decode the remote players data and update 

                    actRef, pos, vel = decode (data)
                    
                    if actRef not in  self.remote_eggs:
                        
                        tmp_egg = egg (self.level)
                        print 'XXX'
                        tmp_egg.tex = self.tex
                        tmp_egg.oval.pos = pos
                        tmp_egg.oval.size = vector (12, 15)
                        tmp_egg.vel = vel
                        self.level.actors.append (tmp_egg)
                        
                        self.remote_eggs[actRef] = tmp_egg

                    eggAct = self.remote_eggs[actRef]
                    eggAct.oval.pos = pos
                    eggAct.vel = vel

                    # send our data to the server, encoded
                    s.send (encode (self.local_egg))
                else:
                    print 'FlailClient: someone snuck into our inputs...'

        if s:
            s.close ()

def packet_size():
    return calcsize ('Idddd')

def decode(data):
    ref,x,y,dx,dy = unpack ('Idddd',data)

    pos = vector (x,y)
    vel = vector (dx,dy)

    return ref, pos, vel

def encode(act):
    x = act.oval.pos.x
    y = act.oval.pos.y
    dx = act.vel.x
    dy = act.vel.y

    return pack ('dddd',x,y,dx,dy)
