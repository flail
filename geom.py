from __future__ import division



class vector (object):

  __slots__ = 'x', 'y', 'z'

  def __init__ (self, x = 0, y = 0, z = 0):
    self.x, self.y, self.z = x, y, z

  def __str__ (self):
    return '(%f, %f, %f)' % (self.x, self.y, self.z)

  __repr__ = __str__

  def __add__ (self, other):
    return vector (self.x + other.x,
                   self.y + other.y,
                   self.z + other.z)

  def __sub__ (self, other):
    return vector (self.x - other.x,
                   self.y - other.y,
                   self.z - other.z)

  def __mul__ (self, other):
    return vector (self.x * other,
                   self.y * other,
                   self.z * other)

  def __truediv__ (self, other):
    return vector (self.x / other,
                   self.y / other,
                   self.z / other)

  __div__ = __truediv__

  def __rmul__ (self, other):
    return vector (self.x * other,
                   self.y * other,
                   self.z * other)

  def __iadd__ (self, other):
    self.x += other.x
    self.y += other.y
    self.z += other.z
    return self

  def __isub__ (self, other):
    self.x -= other.x
    self.y -= other.y
    self.z -= other.z
    return self

  def __imul__ (self, other):
    self.x *= other
    self.y *= other
    self.z *= other
    return self

  def __itruediv__ (self, other):
    self.x /= other
    self.y /= other
    self.z /= other
    return self

  __idiv__ = __itruediv__

  def __neg__ (self):
    return vector (-self.x, -self.y, -self.z)

  def __pos__ (self):
    return self

  def __abs__ (self):
    return self.norm ()

  def norm (self):
    from math import sqrt
    return sqrt (self.normsq ())

  def normsq (self):
    return self.dot (self)

  def normed (self, norm):
    return self * (norm / abs (self))

  def dot (self, other):
    return (self.x * other.x +
            self.y * other.y +
            self.z * other.z)

  def cross (self, other):
    return vector (self.y * other.z - self.z * other.y,
                   self.z * other.x - self.x * other.z,
                   self.x * other.y - self.y * other.x)

  def proj (self, other):
    return other * self.dot (other) / other.dot (other)

  def xrot (self, angle):
    from math import cos, sin
    cosx, sinx = cos (angle), sin (angle)
    return vector (self.x,
                   cosx * self.y - sinx * self.z,
                   sinx * self.y + cosx * self.z)

  def yrot (self, angle):
    from math import cos, sin
    cosy, siny = cos (angle), sin (angle)
    return vector (siny * self.z + cosy * self.x,
                   self.y,
                   cosy * self.z - siny * self.x)

  def zrot (self, angle):
    from math import cos, sin
    cosz, sinz = cos (angle), sin (angle)
    return vector (cosz * self.x - sinz * self.y,
                   sinz * self.x + cosz * self.y,
                   self.z)

  def transform (self, oval):
    # pos = (self - oval.pos).zrot (-oval.angle)
    pos = self - oval.pos
    pos.x /= oval.size.x
    pos.y /= oval.size.y
    return pos

  def invert (self, oval):
    x = self.x * oval.size.x
    y = self.y * oval.size.y
    pos = vector (x, y, self.z)
    return oval.pos + pos
    # return oval.pos + pos.zrot (oval.angle)



class line (object):

  __slots__ = 'p1', 'p2'

  def __init__ (self, p1 = None, p2 = None):
    self.p1 = p1 or vector ()
    self.p2 = p2 or vector ()

  def __call__ (self, t):
    return self.p1 + t * self.dir ()

  def dir (self):
    return self.p2 - self.p1

  def perp (self):
    dir = self.dir ()
    return vector (-dir.y, dir.x)

  def length (self):
    return abs (self.dir ())

  def dist (self):
    cross = self.p1.cross (self.p2)
    return cross.z / self.length ()

  def distsq (self):
    cross = self.p1.cross (self.p2)
    return cross.z * cross.z / self.dir ().normsq ()

  def units (self):
    '''Get parameters yielding points on the unit circle.

    In some sense, finds the intersection(s) of this line
    with the unit circle.  It really only gives the
    solutions to the equation |p1 + t (p2 - p1)| = 1.'''

    from math import sqrt

    dir = self.dir ()
    a = dir.normsq ()
    b = 2 * self.p1.dot (dir)
    c = self.p1.normsq () - 1
    dis = b * b - 4 * a * c

    if dis < 0:
      return ()
    elif dis == 0:
      return (-b / (2 * a),)
    else:
      return ((-b - sqrt (dis)) / (2 * a),
              (-b + sqrt (dis)) / (2 * a))

  def transform (self, oval):
    return line (self.p1.transform (oval),
                 self.p2.transform (oval))

  def invert (self, oval):
    return line (self.p1.invert (oval),
                 self.p2.invert (oval))



class rect (object):

  __slots__ = 'pos', 'size'

  def __init__ (self, pos = None, size = None):
    self.pos  = pos  or vector ()
    self.size = size or vector ()

  def left (self):
    return self.pos.x - self.size.x

  def right (self):
    return self.pos.x + self.size.x

  def top (self):
    return self.pos.y + self.size.y

  def bottom (self):
    return self.pos.y - self.size.y

  def width (self):
    return 2 * self.size.x

  def height (self):
    return 2 * self.size.y

  def set_left (self, l):
    self.pos.x = l + self.size.x

  def set_right (self, r):
    self.pos.x = r - self.size.x

  def set_top (self, t):
    self.pos.y = t - self.size.y

  def set_bottom (self, b):
    self.pos.y = b + self.size.y

  def set_width (self, w):
    self.size.x = w / 2

  def set_height (self, h):
    self.size.y = h / 2



class ellipse (object):

  __slots__ = 'pos', 'size', 'angle'

  def __init__ (self, pos = None, size = None, angle = 0):
    self.pos   = pos  or vector ()
    self.size  = size or vector ()
    self.angle = angle
