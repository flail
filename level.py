from __future__   import division
from xml.sax      import ContentHandler
from pyglet.gl.gl import *
from tracker      import tracker
from geom         import vector, rect
from wall         import wall



import pyglet



VIEW_ACCELERATION = 1000
VIEW_X_TOLERANCE  = 5
VIEW_V_TOLERANCE  = 50



class handler (ContentHandler):

  def __init__ (self, lvl):
    ContentHandler.__init__ (self)

    self.lvl      = lvl
    self.trace    = []
    self.__top    = 0
    self.__left   = 0
    self.__right  = 0
    self.__bottom = 0

  def startElement (self, name, attrs):
    if name in ('level', ):
      if self.trace:
        raise SAXParseException ('<level> unexpected')

    elif name in ('area', 'view', 'bg', 'gravity', 'walls', 'actors'):
      if self.trace != ['level']:
        raise SAXParseException ('<%s> unexpected' % name)

    elif name in ('wall', ):
      if self.trace != ['level', 'walls']:
        raise SAXParseException ('<%s> unexpected' % name)

      self.lvl.walls.append (wall ())

    elif name in ('top', 'left', 'right', 'bottom'):
      if self.trace != ['level', 'area']:
        raise SAXParseException ('<%s> unexpected' % name)

    elif name in ('x', 'y'):
      if self.trace != ['level', 'view']:
        raise SAXParseException ('<%s> unexpected' % name)

    elif name in ('width', 'height'):
      if not self.trace or self.trace[-1] not in ('bg', ):
        raise SAXParseException ('<%s> unexpected' % name)

    elif name in ('x1', 'y1', 'x2', 'y2'):
      if self.trace != ['level', 'walls', 'wall']:
        raise SAXParseException ('<%s> unexpected' % name)

    elif name in ('img', ):
      if not self.trace or self.trace[-1] not in ('bg', 'wall'):
        raise SAXParseException ('<%s> unexpected' % name)

    elif name in ('bounce', 'friction', 'stiction'):
      if self.trace != ['level', 'walls', 'wall']:
        raise SAXParseException ('<%s> unexpected' % name)

    else:
      raise SAXParseException ('<%s> tag unknown' % name)

    self.trace.append (name)

  def endElement (self, name):
    if name != self.trace.pop ():
      raise SAXParseException ('</%s> unexpected' % name)

    if name == 'area':
      self.lvl.area.pos.x = (self.__left + self.__right) / 2
      self.lvl.area.pos.y = (self.__bottom + self.__top) / 2
      self.lvl.area.set_width (self.__right - self.__left)
      self.lvl.area.set_height (self.__top - self.__bottom)
    elif name == 'wall':
      w = wall (self.lvl.walls[-1].seg,
                self.lvl.walls[-1].img)
      w.bounce = self.lvl.walls[-1].bounce
      w.friction = self.lvl.walls[-1].friction
      w.stiction = self.lvl.walls[-1].stiction
      self.lvl.walls[-1] = w

  def characters (self, content):
    if not self.trace:
      raise SAXParseException ('tag missing')

    if self.trace[-1] == 'top':
      if self.trace[-2] == 'area':
        self.__top = float (content)

    elif self.trace[-1] == 'left':
      if self.trace[-2] == 'area':
        self.__left = float (content)

    elif self.trace[-1] == 'right':
      if self.trace[-2] == 'area':
        self.__right = float (content)

    elif self.trace[-1] == 'bottom':
      if self.trace[-2] == 'area':
        self.__bottom = float (content)

    elif self.trace[-1] == 'x':
      if self.trace[-2] == 'view':
        self.lvl.view.pos.x = tracker (VIEW_ACCELERATION,
                                       VIEW_X_TOLERANCE,
                                       VIEW_V_TOLERANCE)

        self.lvl.view.pos.x.x = float (content)
        self.lvl.view.pos.x.y = float (content)

    elif self.trace[-1] == 'y':
      if self.trace[-2] == 'view':
        self.lvl.view.pos.y = tracker (VIEW_ACCELERATION,
                                       VIEW_X_TOLERANCE,
                                       VIEW_V_TOLERANCE)

        self.lvl.view.pos.y.x = float (content)
        self.lvl.view.pos.y.y = float (content)

    elif self.trace[-1] == 'width':
      if self.trace[-2] == 'bg':
        self.lvl.bg_size.x = float (content)

    elif self.trace[-1] == 'height':
      if self.trace[-2] == 'bg':
        self.lvl.bg_size.y = float (content)

    elif self.trace[-1] == 'gravity':
      self.lvl.gravity = float (content)

    elif self.trace[-1] == 'x1':
      if self.trace[-2] == 'wall':
        self.lvl.walls[-1].seg.p1.x = float (content)

    elif self.trace[-1] == 'y1':
      if self.trace[-2] == 'wall':
        self.lvl.walls[-1].seg.p1.y = float (content)

    elif self.trace[-1] == 'x2':
      if self.trace[-2] == 'wall':
        self.lvl.walls[-1].seg.p2.x = float (content)

    elif self.trace[-1] == 'y2':
      if self.trace[-2] == 'wall':
        self.lvl.walls[-1].seg.p2.y = float (content)

    elif self.trace[-1] == 'img':
      if self.trace[-2] == 'bg':
        self.lvl.bg = self.lvl.res.image (content)
      elif self.trace[-2] == 'wall':
        self.lvl.walls[-1].img = self.lvl.res.texture (content)

    elif self.trace[-1] == 'bounce':
      if self.trace[-2] == 'wall':
        self.lvl.walls[-1].bounce = float (content)

    elif self.trace[-1] == 'friction':
      if self.trace[-2] == 'wall':
        self.lvl.walls[-1].friction = float (content)

    elif self.trace[-1] == 'stiction':
      if self.trace[-2] == 'wall':
        self.lvl.walls[-1].stiction = float (content)



class level (object):

  def __init__ (self, cfg, path = None):
    self.res     = pyglet.resource
    self.cfg     = cfg
    self.area    = rect ()
    self.view    = rect ()
    self.bg      = None
    self.bg_size = vector (1, 1)
    self.gravity = 0
    self.walls   = []
    self.actors  = []
    self.player  = None

    from pyglet.window.key import KeyStateHandler
    self.keys = KeyStateHandler ()
    self.load (path)

  def load (self, path):
    if path:
      from os.path import dirname
      self.res = pyglet.resource.Loader (script_home = dirname (path))

      from xml.sax import parse
      parse (path, handler (self))

  def pressed (self, action):
    '''Check by action if a key is pressed.'''

    from pyglet.window import key
    return self.keys[getattr (key, self.cfg[action])]

  def update (self, dt):
    for actor in self.actors:
      actor.vel.y -= dt * self.gravity
      actor.update (dt)

    self.view.pos.x.x = self.player.oval.pos.x
    self.view.pos.y.x = self.player.oval.pos.y

    self.view.pos.x.update (dt)
    self.view.pos.y.update (dt)

  def render (self):
    if self.view.top () > self.area.top ():
      self.view.pos.y.x = self.area.top () - self.view.size.y
      self.view.pos.y.y = self.area.top () - self.view.size.y
      self.view.pos.y.v = 0

    if self.view.left () < self.area.left ():
      self.view.pos.x.x = self.area.left () + self.view.size.x
      self.view.pos.x.y = self.area.left () + self.view.size.x
      self.view.pos.x.v = 0

    if self.view.right () > self.area.right ():
      self.view.pos.x.x = self.area.right () - self.view.size.x
      self.view.pos.x.y = self.area.right () - self.view.size.x
      self.view.pos.x.v = 0

    if self.view.bottom () < self.area.bottom ():
      self.view.pos.y.x = self.area.bottom () + self.view.size.y
      self.view.pos.y.y = self.area.bottom () + self.view.size.y
      self.view.pos.y.v = 0

    glMatrixMode (GL_PROJECTION)
    glLoadIdentity ()
    glOrtho (self.view.left (),
             self.view.right (),
             self.view.bottom (),
             self.view.top (), -1, 1)

    if self.bg:
      bg_width  = self.bg_size.x * self.view.width  ()
      bg_height = self.bg_size.y * self.view.height ()

      try:
        y = self.view.bottom () - self.area.bottom ()
        y /= self.area.height () - self.view.height ()
        y = (self.area.bottom () + y * (self.area.height () - bg_height))
      except ZeroDivisionError:
        y = self.area.bottom ()

      try:
        x = self.view.left () - self.area.left ()
        x /= self.area.width () - self.view.width ()
        x = (self.area.left () + x * (self.area.width () - bg_width))
      except ZeroDivisionError:
        x = self.area.left ()

      glBindTexture (GL_TEXTURE_2D, self.bg.id)
      glBegin (GL_TRIANGLE_STRIP)
      glColor4d (1, 1, 1, 1)

      glTexCoord2d (self.bg.tex_coords[0],
                    self.bg.tex_coords[1])
      glVertex2d (x, y)

      glTexCoord2d (self.bg.tex_coords[3],
                    self.bg.tex_coords[4])
      glVertex2d (x + bg_width, y)

      glTexCoord2d (self.bg.tex_coords[9],
                    self.bg.tex_coords[10])
      glVertex2d (x, y + bg_height)

      glTexCoord2d (self.bg.tex_coords[6],
                    self.bg.tex_coords[7])
      glVertex2d (x + bg_width, y + bg_height)

      glEnd ()
    else:
      glClear (GL_COLOR_BUFFER_BIT)

    for actor in self.actors:
      actor.render ()

    for wall in self.walls:
      wall.render ()
