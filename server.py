import socket
import select
import sys
from struct import pack, unpack, calcsize

from pyglet.clock import Clock

class telemetry:
    def __init__ (self,ref=None,x=0,y=0,dx=0,dy=0):
        self.ready = False
        self.ref = ref
        self.x = x
        self.y = y
        self.dx = dx
        self.dy = dy

HOST = 'localhost'
PORT = 30000
BACKLOG = 5
RECV_SIZE = 1024
SERVER_HELLO = 'FlailServer'
CLIENT_HELLO = 'FlailClient'

#a list of clients that have been verified using their handshake
clients = {}

serv =  None
try:
    serv = socket.socket (socket.AF_INET, socket.SOCK_STREAM)
    serv.setsockopt (socket.SOL_TCP, socket.TCP_NODELAY, 0)
    serv.bind ((HOST,PORT))
    serv.listen (BACKLOG)
except socket.error, (value,message):
    if serv:
        print 'Closing socket, ',
        serv.close ()
    print 'Could not open socket: ' + message
    sys.exit (1)
except socket.herror:
    if serv:
        serv.close ()
    print 'Couldn\'t get address'
    sys.exit (1)

input = [serv]

def packet_size ():
    return calcsize ('dddd')

def decode(data):
    x, y, dx, dy = unpack ('dddd',data)

    return x,y,dx,dy

def encode(telm):
    ref = telm.ref
    x = telm.x
    y = telm.y
    dx = telm.dx
    dy = telm.dy

    tmp = pack ('Idddd', ref, x, y, dx, dy)
    return tmp


def isHello(str):
    b = str == CLIENT_HELLO
    return b

def isReady(str):
    return str == 'READY'

def isNotReady(str):
    return str == 'NOT READY'


in_game = False


try:
    # pre-game initialization, where players can sign-on to play
    while not in_game:
        inputready,outputready,exceptready = select.select (input,[],[])

        for s in inputready:
            if s == serv:
                if in_game:
                    # if we are in-game, we reject new connections
                    pass
                else:
                    # if server is ready and not already in-game,
                    # we should look for a connection
                    client, address = s.accept ()
                    input.append (client)

            elif s in clients:
                # if a verified client is sending us something:
                # initially it may be a ready/not ready signal
                data = s.recv (RECV_SIZE)

                if data:
                    if isReady (data):
                        clients[s].ready = True
                    elif isNotReady (data):
                        clients[s].ready = False
                    else:
                        print 'Unexpected response from player in pre-game'
                else:
                    print 'No data from client in pre-game'

                # if all clients are ready, start the game!
                if len(clients) > 1 and False not in clients.values():
                    in_game = True
            else:
                # otherwise, a client must be ready, so we process it
                data = s.recv (RECV_SIZE)
                if data and isHello (data):
                    s.send (SERVER_HELLO)
                    clients[s] = telemetry (ref = len(clients))
                else:
                    s.close ()
                    input.remove (s)

    #pre-game setup is over, start to relay messages among players
    print 'Starting game'

    game_over = False
    update_rate = 10
    clk = Clock (update_rate)

    while not game_over:
        clk.tick ()

        for dest in clients:
            for other in clients:
                if dest != other:
                    enc = encode (clients[other])
                    dest.send (enc)

        inputready,outputready,exceptready = select.select (input,[],[])

        for s in inputready:
            if s == serv:
                print 'Server ready'
            if s in clients:
                data = s.recv (packet_size ())

                if data:
                    x,y,dx,dy = decode(data)
                    clients[s].x = x
                    clients[s].y = y
                    clients[s].dx = dx
                    clients[s].dy = dy
                else:
                    input.remove(s)
            else:
                print 'Unknown client in game'

except KeyboardInterrupt:
    if serv:
        print 'Interrupted: closing connection'
        serv.close ()

serv.close ()
