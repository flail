from __future__   import division
from config       import config
from client       import client
from pyglet.gl.gl import *



import pyglet



def main (argv):
  from game import game

  # default configuration settings
  defaults = { 'window.fullscreen' : 'true',
               'action.jump'       : 'W',
               'action.drop'       : 'S',
               'action.left'       : 'A',
               'action.right'      : 'D' }

  cfg = config ('data/config.xml', defaults)
  win = game (cfg)

  glEnable (GL_BLEND)
  glEnable (GL_TEXTURE_2D)
  glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

  # begin cloud
  win.lvl.load ('data/grammar.lvl')
  win.lvl.view.size.x = win.width / 2
  win.lvl.view.size.y = win.height / 2
  win.push_handlers (win.lvl.keys)
  from egg import egg
  from ball import ball
  from geom import vector
  from client import client
  dude = egg (win.lvl)
  dude.tex = pyglet.image.load ('data/egg.png').texture
  dude.oval.pos  = vector (500, 385)
  dude.oval.size = vector (12, 15)
  dude.vel = vector (-10, 0)
  thing = ball (win.lvl, dude)
  thing.oval.pos = vector (300, 400)
  win.lvl.actors.append (dude)
  win.lvl.actors.append (thing)
  win.lvl.player = dude
  cli = client (win.lvl, dude)
  cli.start ()
  # end cloud

  @win.event
  def on_draw ():
    win.lvl.render ()
    win.show_overlay ()

  def update (dt):
    win.lvl.update (dt)

  pyglet.clock.schedule (update)
  pyglet.app.run ()
  cfg.save ()

  cli.running = False



if __name__ == '__main__':
  from sys import argv
  main (argv)
