from __future__ import division
from xml.sax    import ContentHandler



class handler (ContentHandler):

  def __init__ (self, data):
    ContentHandler.__init__ (self)
    self.data  = data
    self.trace = []

  def startElement (self, name, attrs):
    if not self.trace and name != 'config':
      raise SAXParseException ('<%s> unexpected' % name)

    self.trace.append (name)

  def endElement (self, name):
    self.trace.pop ()

  def characters (self, content):
    content = content.strip ()

    if content:
      self.data['.'.join (self.trace[1:])] = content



class config (dict):

  def __init__ (self, path = 'data/config.xml', defaults = {}):
    dict.__init__ (self)
    self.path, self.defaults = path, defaults

    try:
      from xml.sax import parse
      parse (path, handler (self))
    except IOError:
      pass

  def __getitem__ (self, key):
    if dict.__contains__ (self, key):
      return dict.__getitem__ (self, key)
    else:
      return self.defaults[key]

  def save (self, path = None):
    if not path:
      path = self.path

    if not path:
      path = 'data/config.xml'

    file = open (path, 'w')

    try:
      file.write ('<config>\n')

      keys = {}

      for k in self.keys ():
        keys[k] = None

      for k in self.defaults.keys ():
        keys[k] = None

      keys = keys.keys ()
      keys.sort ()
      trace = []

      for k in keys:
        value = self[k]
        path = k.split ('.')

        while trace != path[:len (trace)]:
          file.write (len (trace) * '  ' + '</%s>\n' % trace.pop ())

        path, _, name = k.rpartition ('.')
        path = path.split ('.')

        while trace != path:
          trace.append (path[len (trace)])
          file.write (len (trace) * '  ' + '<%s>\n' % trace[-1])

        file.write (len (trace) * '  ' + '  <%s>%s</%s>\n' %
                    (name, value, name))

      while trace:
        file.write (len (trace) * '  ' + '</%s>\n' % trace.pop ())

      file.write ('</config>\n')
    finally:
      file.close ()
