from __future__    import division
from actor         import actor
from pyglet.window import key
from pyglet.gl.gl  import *



class egg (actor):

  def __init__ (self, owner):
    actor.__init__ (self, owner)
    self.tex = None
    self.dir = 1

  def update (self, dt):
    if self is self.owner.player:
      if self.owner.pressed ('action.drop'):
        self.drop (200 * dt)
      elif self.owner.pressed ('action.left'):
        self.left (400 * dt)
        self.dir = -1
      elif self.owner.pressed ('action.right'):
        self.right (400 * dt)
        self.dir = 1

      self.jump (4000, dt, self.owner.pressed ('action.jump'))

    actor.update (self, dt)

  def render (self):
    from math import pi

    if self.tex:
      glPushMatrix ()
      glTranslated (self.oval.pos.x, self.oval.pos.y, 0)
      glRotated (self.oval.angle * 180 / pi, 0, 0, 1)

      if self.dir < 0:
        glScaled (-1, 1, 1)

      glBindTexture (GL_TEXTURE_2D, self.tex.id)
      glBegin (GL_TRIANGLE_STRIP)
      glColor4d (1, 1, 1, 1)

      glTexCoord2d (self.tex.tex_coords[0],
                    self.tex.tex_coords[1])
      glVertex2d (-16, -16)

      glTexCoord2d (self.tex.tex_coords[3],
                    self.tex.tex_coords[4])
      glVertex2d (16, -16)

      glTexCoord2d (self.tex.tex_coords[9],
                    self.tex.tex_coords[10])
      glVertex2d (-16, 16)

      glTexCoord2d (self.tex.tex_coords[6],
                    self.tex.tex_coords[7])
      glVertex2d (16, 16)

      glEnd ()
      glPopMatrix ()
