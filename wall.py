from __future__   import division
from geom         import line
from pyglet.gl    import *



class wall (object):

  def __init__ (self, seg = None, img = None):
    self.bounce, self.friction, self.stiction = 0, 0, 0
    self.seg, self.__seg, self.tex = seg or line (), None, img and img.texture

    if self.tex:
      try:
        perp = seg.perp ().normed (self.tex.height)
        self.__seg = line (seg.p1 - perp, seg.p2 - perp)

        glBindTexture (GL_TEXTURE_2D, self.tex.id)
        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER)
      except ZeroDivisionError:
        pass

  def render (self):
    if self.__seg:
      u = self.seg.length () / self.tex.width
      glBindTexture (GL_TEXTURE_2D, self.tex.id)
      glBegin (GL_TRIANGLE_STRIP)
      glColor4d (1, 1, 1, 1)

      glTexCoord2d (0, self.tex.tex_coords[10])
      glVertex3d (self.seg.p1.x,
                  self.seg.p1.y,
                  self.seg.p1.z)

      glTexCoord2d (u, self.tex.tex_coords[7])
      glVertex3d (self.seg.p2.x,
                  self.seg.p2.y,
                  self.seg.p2.z)

      glTexCoord2d (0, self.tex.tex_coords[1])
      glVertex3d (self.__seg.p1.x,
                  self.__seg.p1.y,
                  self.__seg.p1.z)

      glTexCoord2d (u, self.tex.tex_coords[4])
      glVertex3d (self.__seg.p2.x,
                  self.__seg.p2.y,
                  self.__seg.p2.z)

      glEnd ()
