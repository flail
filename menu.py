from pyglet import font, image
from pyglet.window import key
from pyglet.gl.gl import *

from geom import vector
from menuitem import menuitem


'''
This class is for maintaining a main menu screen when the game
starts. 
'''

ITEM_HEIGHT = 64
ITEM_SPACE = 15

class menu (object):
    
    def __init__(self,game):
        arial = font.load('Arial',32)
        self.active = False
        self.game = game
        self.curr = 0
        self.listItems = [menuitem('Start')
                          ,menuitem('Load')
                          ,menuitem('Save')
                          ,menuitem('Options')
                          ]
        self.listItems[self.curr].highlight()
        
    def handleKey(self, symbol):
        if symbol == key.UP:
            self.prevItem ()
        elif symbol == key.DOWN:
            self.nextItem ()

    def get_current(self):
        return self.listItems[self.curr]

    def prevItem(self):
        self.get_current().unhighlight()
        self.curr = (self.curr - 1) % self.list_size()
        self.get_current().highlight()

    def nextItem(self):
        self.get_current ().unhighlight ()
        self.curr = (self.curr + 1) % self.list_size()
        self.get_current ().highlight ()

    def list_size (self):
        return len(self.listItems)

    def render(self):
        total_height = self.list_size() * ITEM_HEIGHT + \
                       (self.list_size() - 1) * ITEM_SPACE

        glTranslated (0, (total_height - ITEM_HEIGHT) / 2, 0)

        for item in self.listItems:
            item.render ()
            glTranslated (0, -(ITEM_SPACE+ITEM_HEIGHT), 0)
