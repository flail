from __future__ import division
from geom       import vector, line, ellipse



EPSILON     = 0.00001
TOLERANCE   = 0.01
JUMP_RANGE  = 0.5
MIN_BOUNCE  = 20
MAX_AIRTIME = 0.10



class actor (object):

  def __init__ (self, owner):
    self.owner     = owner
    self.oval      = ellipse ()
    self.vel       = vector ()
    self.spin      = 0
    self.surf      = None
    self.__jump    = False
    self.__airtime = None

  def drop (self, dv):
    self.vel.y -= dv

  def jump (self, v, dt, key):
    if self.surf:
      seg1 = self.surf.seg

      if key:
        # key is pressed
        if self.__airtime is None:
          if not self.__jump:
            seg2 = seg1.transform (self.oval)

            if abs (seg2.dist () - 1) < JUMP_RANGE:
              self.vel += seg1.perp ().normed (v * dt)
              self.__airtime = dt
        elif self.__airtime < MAX_AIRTIME:
          self.vel += seg1.perp ().normed (v * dt)
          self.__airtime += dt
      elif self.__airtime is not None:
        # key is released
        seg2 = seg1.perp ()
        self.vel += seg2.normed (v * (self.__airtime - MAX_AIRTIME) / 2)
        self.__airtime = None

    self.__jump = key

  def left (self, dv):
    self.vel.x -= dv

  def right (self, dv):
    self.vel.x += dv

  def update (self, dt):
    from copy import deepcopy
    from wall import wall

    surf, contact, frac = None, None, 1

    oval = deepcopy (self.oval)
    oval.pos += dt * self.vel
    oval.angle += dt * self.spin

    for w in self.owner.walls:
      line1 = w.seg.transform (self.oval)
      line2 = w.seg.transform (oval)

      dist1 = line1.dist ()
      dist2 = line2.dist ()

      if dist2 > 1 + TOLERANCE or dist2 > dist1:
        # actor is above wall or moving up
        continue

      # trajectories of the endpoints
      line3 = line (line1.p1, line2.p1)
      line4 = line (line1.p2, line2.p2)

      try:
        dist3 = line3.dist ()

        if dist3 > 1:
          # actor is beyond the left edge
          continue
        elif dist1 > 0 and dist3 > -1:
          # actor might touch the left edge
          units = [t for t in line3.units () if 0 <= t < frac]

          for t in units:
            if t < frac:
              surf, contact, frac = w, line3 (t), t
      except ZeroDivisionError:
        pass

      try:
        dist4 = line4.dist ()

        if dist4 < -1:
          # actor is beyond the right edge
          continue
        elif dist1 > 0 and dist4 < 1:
          # actor might touch the right edge
          units = [t for t in line4.units () if 0 <= t < frac]

          for t in units:
            if t < frac:
              surf, contact, frac = w, line4 (t), t
      except ZeroDivisionError:
        pass

      if dist1 < 1 - TOLERANCE:
        continue

      try:
        # actor is in the middle
        t = (1 + TOLERANCE - dist1) / (dist2 - dist1)

        if t < frac:
          tline = line (line3 (t), line4 (t))
          tpoint = tline.p1.proj (tline.perp ())

          if (tpoint.cross (tline.p1).z < 0 and
              tpoint.cross (tline.p2).z > 0):
            # actor is definitely touching wall
            surf, contact, frac = w, tpoint, t
      except ZeroDivisionError:
        pass

    self.oval.pos += frac * dt * self.vel
    self.oval.angle += frac * dt * self.spin

    if isinstance (surf, wall):
      try:
        dir = self.vel.proj (surf.seg.dir ())

        perp = surf.seg.perp ()
        self.oval.pos += perp.normed (EPSILON)
        perp = self.vel.proj (perp)
        norm = abs (perp)
        perp = -surf.bounce * perp

        if perp.normsq () < MIN_BOUNCE * MIN_BOUNCE:
          speed = abs (dir)

          if speed < surf.stiction * norm:
            speed = 0
          else:
            speed -= surf.friction * norm

          try:
            dir = dir.normed (speed)
          except ZeroDivisionError:
            pass

          perp.x, perp.y = 0, 0

        self.surf = surf
        self.vel = perp + dir
        actor.update (self, dt * (1 - TOLERANCE - frac))
      except ZeroDivisionError:
        pass

  def render (self):
    raise NotImplementedError ('actor.render')
