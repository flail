from __future__   import division
from actor        import actor
from pyglet.gl.gl import *



import pyglet



class ball (actor):

  def __init__ (self, owner, user):
    actor.__init__ (self, owner)

    self.user = user

    self.tex = owner.res.image ('ball.png')
    self.oval.size.x = self.tex.width  / 2
    self.oval.size.y = self.tex.height / 2

  def render (self):
    if self.tex:
      glPushMatrix ()
      glTranslated (self.oval.pos.x, self.oval.pos.y, 0)

      glBindTexture (GL_TEXTURE_2D, self.tex.id)
      glBegin (GL_TRIANGLE_STRIP)
      glColor4d (1, 1, 1, 1)

      glTexCoord2d (self.tex.tex_coords[0],
                    self.tex.tex_coords[1])
      glVertex2d (-self.oval.size.x,
                  -self.oval.size.y)

      glTexCoord2d (self.tex.tex_coords[3],
                    self.tex.tex_coords[4])
      glVertex2d ( self.oval.size.x,
                  -self.oval.size.y)

      glTexCoord2d (self.tex.tex_coords[9],
                    self.tex.tex_coords[10])
      glVertex2d (-self.oval.size.x,
                   self.oval.size.y)

      glTexCoord2d (self.tex.tex_coords[6],
                    self.tex.tex_coords[7])
      glVertex2d ( self.oval.size.x,
                   self.oval.size.y)

      glEnd ()
      glPopMatrix ()
