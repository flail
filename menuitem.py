from pyglet       import font,image
from pyglet.gl.gl import *

from geom import vector

class menuitem (object):
    def __init__(self,str,x=0,y=0):
        self.label = str
        self.pos = vector (x,y)
        self.bg = image.load('data/menuitem.png').texture
        self.is_highlighted = False
        self.alpha = 0.1
        self.text = font.Text(font.load('Arial',32),
                              str,
                              x = x,
                              y = y,
                              valign = 'center',
                              halign = 'center'
                              )
                              
        glBindTexture   (GL_TEXTURE_2D, self.bg.id)
        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER)
        glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER)

    def highlight(self):
        self.is_highlighted = True
        self.alpha = 0.3

    def unhighlight(self):
        self.is_highlighted = False
        self.alpha = 0.1

    def render (self):
        glBindTexture (GL_TEXTURE_2D, self.bg.id)
        glBegin (GL_TRIANGLE_STRIP)

        glColor4d (0.0, 0.0, 1.0, self.alpha)
        glTexCoord2d (self.bg.tex_coords[0],
                      self.bg.tex_coords[1])
        glVertex2d (-self.bg.width  / 2, 
                    -self.bg.height / 2)

        glTexCoord2d (self.bg.tex_coords[3],
                      self.bg.tex_coords[4])
        glVertex2d ( self.bg.width  / 2, 
                    -self.bg.height / 2)

        glTexCoord2d (self.bg.tex_coords[9],
                      self.bg.tex_coords[10])
        glVertex2d (-self.bg.width  / 2, 
                     self.bg.height / 2)

        glTexCoord2d (self.bg.tex_coords[6],
                      self.bg.tex_coords[7])
        glVertex2d ( self.bg.width  / 2, 
                     self.bg.height / 2)

        glEnd ()

        self.text.draw ()
