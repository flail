from pyglet.window import Window,key
from pyglet.gl.gl import *

from menu import *



import pyglet



class game (Window):

    def __init__ (self, cfg):
        Window.__init__ (self,
            fullscreen = cfg['window.fullscreen'] != 'false')

        from level import level
        self.menu = menu (self)
        self.lvl  = level (cfg)
        self.cfg  = cfg

    '''
    More keypress stuff goes here. For example a key
    to bring up the overlay, relay key events to the
    game-in-progress, etc. 
    '''    
    def on_key_press(self,symbol,mod):
        if symbol == key.ESCAPE:
            self.quit_game ()
        elif symbol == key.ASCIITILDE:
            self.menu.active = not self.menu.active
        elif self.menu.active:
            self.menu.handleKey (symbol)

    '''
    Resize events should be propegated to the level, so it knows that the
    view size has changed.
    '''

    #def on_resize(self, size, ):
    #    pass


    '''
    Routine to shutdown the game.
    '''
    def quit_game(self):
        pyglet.app.exit ()


    '''
    Bring up the overlay for the game, including
    save-game,load-game, exit, options, and anything
    else that may be needed.
    '''

    def show_overlay (self):
        self.pause_game()

        if self.menu.active:
          glPushMatrix ()
          glLoadIdentity ()

          glBindTexture (GL_TEXTURE_2D, 0)
          glBegin (GL_TRIANGLE_STRIP)
          glColor4d (0, 0, 0, 0.5)

          glVertex2d (-1, -1)
          glVertex2d ( 1, -1)
          glVertex2d (-1,  1)
          glVertex2d ( 1,  1)

          glEnd ()

          glOrtho (-self.width  / 2,
                    self.width  / 2,
                   -self.height / 2,
                    self.height / 2, -1, 1)

          self.menu.render ()

          glPopMatrix ()

    '''
    This should most likely disconnect the 'time' handler
    from the 'time' events. This way the game doesn't know that
    time is passing.
    '''

    def pause_game (self):
        pass
